function validate() {
    var ccNumber = document.getElementById("ccNumber").value;

    var visa = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
    var amex = new RegExp("^3[47][0-9]{13}$");
    var mastercard = new RegExp("^5[1-5][0-9]{14}$");
    var discover = new RegExp("^6011[0-9]{12}");

    var visaStart = new RegExp("^4");
    var amexStart = new RegExp("^3");
    var mastercardStart = new RegExp("^5");
    var discoverStart = new RegExp("^6011");

      if (visaStart.test(ccNumber)) {
        document.getElementById("card__logo").innerHTML = "<img src='img/visa.png' />";
        return true;

      } else if (mastercardStart.test(ccNumber)) {
        document.getElementById("card__logo").innerHTML = "<img src='img/mastercard.png' />";
        return true;

      } else if (amexStart.test(ccNumber)) {
        document.getElementById("card__logo").innerHTML = "<img src='img/amex.png' />";
        return true;

      } else if (discoverStart.test(ccNumber)) {
        document.getElementById("card__logo").innerHTML = "<img src='img/discover.png' />";
        return true;

      }

        document.getElementById("card__logo").innerHTML = "";
      return false
    }
