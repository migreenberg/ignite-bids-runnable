@extends('layouts.app')

@section('content')


    <div class="panel-heading">Enter Credit Card Info</div>

<div class="grid align__item">


   <div class="card">

     <header class="card__header">
       <h3 class="card__title">Credit Card Details</h3>
            <div id="card__logo"></div>
     </header>

     <form action="" method="post" class="form">

       <div class="name form__field">
         <label for="name" class="name_label">Name on Card</label>
           <input type="text" name="name" id="name" placeholder="Joe Cool" />
          </div>

       <div class="ccNumber form__field">
         <label for="ccNumber" class="ccNumber_label">Card Number</label>
           <input type="text" name="ccNumber" id="ccNumber" oninput="validate();" placeholder="4000 1234 5678 9010" />
             </div>


       <div class="card__expiration form__field">
         <label for="card__expiration__year">Expiration</label>
         <select name="" id="card__expiration__year">
           <option value="january">January</option>
           <option value="februrary">Februrary</option>
           <option value="march">March</option>
           <option value="april">April</option>
           <option value="may">May</option>
           <option value="june">June</option>
           <option value="july">July</option>
           <option value="august">August</option>
           <option value="september">September</option>
           <option value="october">October</option>
           <option value="november">November</option>
           <option value="december">December</option>
         </select>

         <select name="" id="">
           <option value="2017">2017</option>
           <option value="2018">2018</option>
           <option value="2019">2019</option>
           <option value="2020">2020</option>
           <option value="2021">2021</option>
         </select>
       </div>

       <div class="card__ccv form__field">
         <label for="">CVC</label>
         <input type="text" class="card__ccv__input" placeholder="111">
       </div>

     </form>

   </div>


 </div>


<br/>

  <div class="panel panel-default">

      <div class="panel-body"><form role="form" method="POST"action="http://localhost:8080/register" class="form-horizontal">

                          <div id="cardFooter"> <a href="{{ url('/step3') }}">Proceed to Step 3</a></div>
                        </div>
                      </form>
                    </div>
                  </div>
@endsection
