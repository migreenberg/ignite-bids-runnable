<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/auth/{provider}', 'Auth\LoginController@redirectToProvider')->name('authenticate.with.provider');
Route::get('/auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/home', 'HomeController@index');
Route::get('/step2', function(){return View::make('step2');});
Route::get('/step3', function(){return View::make('step3');});
Route::get('/step4', function(){return View::make('step4');});
