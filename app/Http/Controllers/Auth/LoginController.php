<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Http\Request;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function redirectToProvider($provider)
    {
        if(!in_array($provider, ['twitter', 'google'])) {
            return redirect('login')->withErrors(['social' => 'This provider is not supported.']);
        }
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback(Request $request, $provider)
    {
        try {
            if (! $request->has('code')) {
                throw new Exception('Could not authenticate your ' . ucfirst($provider) . ' account.');
            }
            $social = Socialite::driver($provider)->user();

            $user = $this->findOrCreateUser($social, $provider);

            Auth::login($user, true);

            return redirect($this->redirectTo);
        }
        catch(Exception $e) {
            return redirect('login')->withErrors(['social' => $e->getMessage()]);
        }

    }

    private function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider', $provider)->where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        if(User::where('email', $user->email)->first()) {
            throw new Exception('This account is associated with an email address already in use. To log in using ' . ucfirst($provider) . ', you\'ll need to log in with your username and password first.');
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }

    public function username()
    {
        return 'username';
    }
}
